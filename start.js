'use strict'
const ini = require('ini'), fs = require('fs')
const config = ini.parse(fs.readFileSync('config.ini', 'utf-8'))


var poloniex = require('./api/Poloniex')


poloniex.setKeys(config.poloniex.api_key, config.poloniex.secret)
poloniex.returnBalances()
