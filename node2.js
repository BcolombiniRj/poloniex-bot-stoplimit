const RxHttpRequest = require('rx-http-request').RxHttpRequest;

RxHttpRequest.get('http://www.google.fr').subscribe(success,err);


function success(data){
  if (data.response.statusCode === 200) {
      console.log(data.body); // Show the HTML for the Google homepage.
  }
}

function err(err) {
  console.log(err);
}
