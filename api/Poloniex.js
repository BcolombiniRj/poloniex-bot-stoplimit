const RxHttpRequest = require('rx-http-request').RxHttpRequest;
const crypto = require('crypto')
const baseRequest = RxHttpRequest.defaults({
    headers: {
        "Sign": "",
        "Key": ""
    }
});
const url = "https://poloniex.com/"
const extend = require("extend");
var response = null;

exports.setKeys = function (APIKey, secret) {
    apikey = APIKey
    secretkey = secret
}

api_query = function (command, body) {
    if (command == "returnTicker" || command == "return24Volume") {

    } else if (command == "returnOrderBook") {

    } else if (command == "returnMarketTradeHistory") {

    } else {

        baseRequest.defaults({
            headers: {
                "Sign": crypto.createHash('sha512').update(new Buffer(secretkey, "hex")).digest('hex'),
                "Key": apikey
            }
        })
        console.log(crypto.createHash('sha512').update(new Buffer(secretkey, "hex")).digest('hex'))
        console.log(apikey)
        options = {
            body: {
                command: command,
            },
            json: true
        }
        extend(options.body,body)
        baseRequest.post(url + '/tradingApi', options).subscribe(successRequest, failRequest);
    }

}
exports.returnTicker = function () {
    return api_query("returnTicker")
}

exports.return24Volume = function () {
    return api_query("return24Volume")
}

exports.returnOrderBook = function (currencyPair) {
    return api_query("returnOrderBook", {'currencyPair': currencyPair})
}

exports.returnMarketTradeHistory = function (currencyPair) {
    return api_query("returnMarketTradeHistory", {'currencyPair': currencyPair})
}
// Returns all of your balances.
// Outputs:
// {"BTC":"0.59098578","LTC":"3.31117268", ... }

exports.returnBalances = function () {
    return api_query('returnBalances')
}

// Returns your open orders for a given market, specified by the "currencyPair" POST parameter, e.g. "BTC_XCP"
// Inputs:
// currencyPair  The currency pair e.g. "BTC_XCP"
// Outputs:
// orderNumber   The order number
// type          sell or buy
// rate          Price the order is selling or buying at
// Amount        Quantity of order
// total         Total value of order (price * quantity)
exports.returnOpenOrders = function (currencyPair) {
    return api_query('returnOpenOrders', {currencyPair: currencyPair})
}
// Returns your trade history for a given market, specified by the "currencyPair" POST parameter
// Inputs:
// currencyPair  The currency pair e.g. "BTC_XCP"
// Outputs:
// date          Date in the form: "2014-02-19 03:44:59"
// rate          Price the order is selling or buying at
// amount        Quantity of order
// total         Total value of order (price * quantity)
// type          sell or buy
exports.returnTradeHistory = function (currencyPair) {
    return api_query('returnTradeHistory', {currencyPair: currencyPair})
}
// Places a buy order in a given market. Required POST parameters are "currencyPair", "rate", and "amount". If successful, the method will return the order number.
// Inputs:
// currencyPair  The curreny pair
// rate          price the order is buying at
// amount        Amount of coins to buy
// Outputs:
// orderNumber   The order number
exports.buy = function (currencyPair, rate, amount) {
    return api_query('buy', {currencyPair: currencyPair, rate: rate, amount: amount})
}
// Places a sell order in a given market. Required POST parameters are "currencyPair", "rate", and "amount". If successful, the method will return the order number.
// Inputs:
// currencyPair  The curreny pair
// rate          price the order is selling at
// amount        Amount of coins to sell
// Outputs:
// orderNumber   The order number
exports.sell = function (currencyPair, rate, amount) {
    return api_query('sell', {currencyPair: currencyPair, rate: rate, amount: amount})
}
// Cancels an order you have placed in a given market. Required POST parameters are "currencyPair" and "orderNumber".
// Inputs:
// currencyPair  The curreny pair
// orderNumber   The order number to cancel
// Outputs:
// succes        1 or 0
exports.cancel = function (currencyPair, orderNumber) {
    return api_query('cancelOrder', {currencyPair: currencyPair, orderNumber: orderNumber})
}
// Immediately places a withdrawal for a given currency, with no email confirmation. In order to use this method, the withdrawal privilege must be enabled for your API key. Required POST parameters are "currency", "amount", and "address". Sample output: {"response":"Withdrew 2398 NXT."}
// Inputs:
// currency      The currency to withdraw
// amount        The amount of this coin to withdraw
// address       The withdrawal address
// Outputs:
// response      Text containing message about the withdrawal

exports.withdraw = function (currency, amount, address) {
    return api_query('withdraw', {currency: currency, amount: amount, address: address})
}

successRequest = function (data) {
    console.log(data.body)
    // response = JSON.parse(data.body);
}

failRequest = function (err) {
    console.log(err)
}

exports.return = function(){
    return response;
}

